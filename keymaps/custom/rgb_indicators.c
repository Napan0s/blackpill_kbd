
/* Key led positions
 * ,--------------------------------------------------------------------------------------------------------.
 * |   0  |   1  |   2  |   3  |   4  |   5  |   6  |   7  |   8  |   9  |  10  |  11  |  12  |  13  |  14  |
 * |------+------+------+------+------+------+------+------+------+------+------+------+------+------+------|
 * |  29  |  28  |  27  |  26  |  25  |  24  |  23  |  22  |  21  |  20  |  19  |  18  |  17  |  16  |  15  |
 * |------+------+------+------+------+-------------+------+------+------+------+------+------+------+------|
 * |  30  |  31  |  32  |  33  |  34  |  35  |  36  |  37  |  38  |  39  |  40  |  41  |  42  |  43  |  44  |
 * |------+------+------+------+------+------|------+------+------+------+------+------+------+------+------|
 * |  59  |  58  |  57  |  56  |  55  |  54  |  53  |  52  |  51  |  50  |  49  |  48  |  47  |  46  |  45  |
 * |------+------+------+------+------+------+------+------+------+------+------+------+------+------+------|
 * |  60  |  61  |  62  |  63  |  64  |  65  |  66  |  67  |  68  |  69  |  70  |  71  |  72  |  73  |  74  |
 * `--------------------------------------------------------------------------------------------------------'
 */

void rgb_matrix_indicators_kb(void)
{
	if (!g_suspend_state && rgb_matrix_config.enable) {
        int val = (rgb_matrix_get_val()*0.8);
        switch (get_highest_layer(layer_state)) {
            //case _qwerty:
            //    rgb_matrix_set_color(26, 0xFF, 0x00, 0x00);
            //    break;
           case _SECOND:
                rgb_matrix_set_color(1, val, val, val);
                rgb_matrix_set_color(2, val, val, val);
                rgb_matrix_set_color(3, val, val, val);
                rgb_matrix_set_color(4, val, val, val);
                rgb_matrix_set_color(5, val, val, val);
                rgb_matrix_set_color(6, val, val, val);
                rgb_matrix_set_color(7, val, val, val);
                rgb_matrix_set_color(8, val, val, val);
                rgb_matrix_set_color(9, val, val, val);
                rgb_matrix_set_color(10, val, val, val);
                rgb_matrix_set_color(19, val, val, val);
                
                rgb_matrix_set_color(20, 0, val*1.5, 0);
                rgb_matrix_set_color(21, 0, val*1.5, 0);
                rgb_matrix_set_color(22, 0, val*1.5, 0);
                rgb_matrix_set_color(23, 0, val*1.5, 0);
                rgb_matrix_set_color(24, 0, val*1.5, 0);
                rgb_matrix_set_color(25, 0, val*1.5, 0);
                rgb_matrix_set_color(26, 0, val*1.5, 0);
                rgb_matrix_set_color(27, 0, val*1.5, 0);

                rgb_matrix_set_color(11, val, 0, val);
                rgb_matrix_set_color(12, val, 0, val);
                rgb_matrix_set_color(13, val, 0, val);
                rgb_matrix_set_color(14, val, 0, val);
                rgb_matrix_set_color(15, val, 0, val);
                rgb_matrix_set_color(16, val, 0, val);
                rgb_matrix_set_color(17, val, 0, val);
                rgb_matrix_set_color(18, val, 0, val);
                rgb_matrix_set_color(41, val, 0, val);
                rgb_matrix_set_color(42, val, 0, val);
                rgb_matrix_set_color(43, val, 0, val);
                rgb_matrix_set_color(44, val, 0, val);
                rgb_matrix_set_color(45, val, 0, val);
                rgb_matrix_set_color(46, val, 0, val);
                rgb_matrix_set_color(47, val, 0, val);
                rgb_matrix_set_color(48, val, 0, val);
                rgb_matrix_set_color(72, val, 0, val);
                rgb_matrix_set_color(73, val, 0, val);
                rgb_matrix_set_color(74, val, 0, val);
                break;
            case _ACCENTED:
                rgb_matrix_set_color(20, val, val, val);
                rgb_matrix_set_color(21, val, val, val);
                rgb_matrix_set_color(22, val, val, val);
                rgb_matrix_set_color(26, val, val, val);
                rgb_matrix_set_color(31, val, val, val);
                break;
            case _RGB:
                rgb_matrix_set_color(0, val, val, val);
                rgb_matrix_set_color(1, val, val, val);
                rgb_matrix_set_color(2, val, val, val);
                rgb_matrix_set_color(3, val, val, val);
                rgb_matrix_set_color(4, val, val, val);
                rgb_matrix_set_color(5, val, val, val);
                rgb_matrix_set_color(24, val, val, val);
                rgb_matrix_set_color(25, val, val, val);
                rgb_matrix_set_color(26, val, val, val);
                rgb_matrix_set_color(27, val, val, val);
                rgb_matrix_set_color(28, val, val, val);
                break;
            case _LOWER:
                rgb_matrix_set_color(0, val*1.5, 0, 0);
                rgb_matrix_set_color(29, 0, 0, val*1.5);
                rgb_matrix_set_color(30, 0, val*1.5, 0);
                rgb_matrix_set_color(1, val, val, 0);
                rgb_matrix_set_color(2, val, val, 0);
                rgb_matrix_set_color(3, val, val, 0);
                rgb_matrix_set_color(4, val, val, 0);
                rgb_matrix_set_color(5, val, val, 0);
                rgb_matrix_set_color(6, val, val, 0);
                rgb_matrix_set_color(7, val, val, 0);
                rgb_matrix_set_color(8, val, val, 0);
                rgb_matrix_set_color(9, val, val, 0);
                rgb_matrix_set_color(10, val, val, 0);
                rgb_matrix_set_color(24, val, val, val);
                rgb_matrix_set_color(27, val, val, val);
                rgb_matrix_set_color(28, val, val, val);
                rgb_matrix_set_color(32, val, val, val);
                rgb_matrix_set_color(33, val, val, val);
                rgb_matrix_set_color(34, val, val, val);
                rgb_matrix_set_color(35, val, val, val);
                rgb_matrix_set_color(21,  0, 0,  val*1.5);
                rgb_matrix_set_color(37,  0, 0,  val*1.5);
                rgb_matrix_set_color(38,  0, 0,  val*1.5);
                rgb_matrix_set_color(39,  0, 0,  val*1.5);
                rgb_matrix_set_color(51, val, val, val);
                rgb_matrix_set_color(53, val, val, val);
                rgb_matrix_set_color(54, val, val, val);
                rgb_matrix_set_color(60, val*1.5, 0, 0);
                rgb_matrix_set_color(46, val*1.5, 0, val*1.5);
                rgb_matrix_set_color(72, val*1.5, 0, val*1.5);
                rgb_matrix_set_color(73, val*1.5, 0, val*1.5);
                rgb_matrix_set_color(74, val*1.5, 0, val*1.5);
                break;
        }
    }
}